package happyfamily;


import java.util.HashMap;
import java.util.Random;

public final class Women extends Human implements HumanCreator {

    public Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Women(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Women() {
        
    }

    @Override
    void greetPet() {
        System.out.println("Привет, "+ getFamily().getPet().iterator().next().getNickname()+".");
    }

    public void makeUp(){
        System.out.println("Надо накраситса, а то хожу как чучело.");
    }

    @Override
    public Human bornChild(String boyName, String girlName) {
        Random random= new Random();
        int sex= random.nextInt(2);

        if (sex==0){
            Women girl = new Women();
            girl.setYear(0);
            girl.setFamily(getFamily());
            girl.setName(girlName);
            girl.setSurname(getFamily().getFather().getSurname());
            girl.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            girl.getFamily().addChild(girl);
            return girl;
        }
        else{
            Man boy = new Man();
            boy.setYear(0);
            boy.setFamily(getFamily());
            boy.setName(boyName);
            boy.setSurname(getFamily().getFather().getSurname());
            boy.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            boy.getFamily().addChild(boy);
            return boy;
        }
    }
}
