package happyfamily;

import java.util.*;

public class Human {

    static{
        System.out.println("Class "+ Human.class.getSimpleName()+" is loading.");
    }

    {
        System.out.println("New " + this.getClass().getSimpleName() +" class creating.");
    }

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<String,String> schedule;
    private Family family;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    void greetPet(){
        System.out.println("Привет, "+ family.getPet().iterator().next().getNickname()+".");
    }

    void describePet(){
        String petTrickLevel;
        if (family.getPet().iterator().next().getTrickLevel()>50){
            petTrickLevel="очень хитрая";
        }
        else {
            petTrickLevel="почти не хитрая";
        }
        System.out.println("У меня есть "+family.getPet().iterator().next().getSpecies()+", ему "+family.getPet().iterator().next().getAge()+" лет, он "+petTrickLevel+".");
    }

    @Override
    protected void finalize(){
        System.out.println(this);
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName()+"{name= '"+name+"', surname= '"+surname+"', year="+year+", iq="+iq+", schedule= "+ schedule+"}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    boolean feedPet(boolean timeToEat){
        Random rndm= new Random();
        int random=rndm.nextInt(101);
        boolean feeding;
        if (timeToEat){
            System.out.println("Хм... покормлю ка я "+family.getPet().iterator().next().getNickname()+".");
            feeding=true;
        }
        else {
            if (random<family.getPet().iterator().next().getTrickLevel()){
                System.out.println("Хм... покормлю ка я "+family.getPet().iterator().next().getNickname());
                feeding=true;

            }
            else{
                System.out.println("Думаю, "+family.getPet().iterator().next().getNickname()+" не голодна.");
                feeding=false;
            }
        }
        return feeding;
    }

    Human(String name, String surname, int year){

        this.name=name;
        this.surname=surname;
        this.year=year;

    }


    public Human(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    Human(){

    }

}
